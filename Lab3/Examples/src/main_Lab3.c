/*============================================================================
 *           LPCXpresso 1343 + Embedded Artists Development Board 
 *---------------------------------------------------------------------------*
 *            Universidade Tecnol�gica Federal do Paran� (UTFPR)
 *                                                                           *
 *              Lab 3: Equipe Vin�cius Sandri Diaz                           *
 *===========================================================================*/

#include "mcu_regs.h"
#include "libdemo.h"
#include "type.h"
#include "uart.h"
#include "stdio.h"
#include "timer32.h"
#include "i2c.h"
#include "gpio.h"
#include "ssp.h"
#include "adc.h"
#include "light.h"
#include "oled.h"
#include "rgb.h"
#include "temp.h"
#include "acc.h"
#include "data.h"
#include "stdbool.h"
#include "diskio.h"
#include "ff.h"
#include "cmsis_os.h"
#define BUFFSIZE 32

const uint32_t ticks_factor = 72000;
static bool fall = false;
static int32_t circBuff[BUFFSIZE];
static char line2[330];
static char linegantt[50];
static char converte[10];


/*Variaveis para armazenar no buffer circular*/
int32_t _x;
int32_t _y;
int32_t _z;
uint32_t _l;
uint32_t time;  

/*Utilizados para calibrar o acelerometro*/
int32_t xoff = 0;
int32_t yoff = 0;
int32_t zoff = 0;

int8_t x = 0;
int8_t y = 0;
int8_t z = 0;
uint32_t lux = 0;
uint32_t timeGanttTimer =0;

static  FIL fil_Grava;         /* File object */
static  FIL fil_Gantt;         /* File object */

/*Variaveis utilizadas em algumas fun��es*/

uint32_t timerAcq[8];
uint8_t i;
uint8_t countBuf = 0;
uint8_t count = 0;
uint8_t posiUltimo = 0;
/**/
/*Fun��es Implementadas no Projeto*/
void inicia(void);                                                              /*Inicia fun��es da placa*/
void gravaSD(void const *args);                                                 /*Fun��o para gravar buufer circular no sd*/
void addBuff(int32_t elemx, int32_t elemy, int32_t elemz,  int32_t eleml);      /*Adiciona m�dia m�vel no buffer circular*/
void mediaMovel(int32_t xs,  int32_t ys,  int32_t zs, int32_t zl);              /*Calcula M�dia M�vel*/
void iniciaDispaly(const bool inicia);                                          /*Inicia o dislplay*/
void atualizaDisplay(void const *args);                                         /*Atualiza o display*/
void setFall(bool _fall);                                                       /*Adiciona esta de placa em queda*/
void contagemFall(void const *args);                                            /*Contagem da placa na vertical*/
void verficaSensoresTimer(void const *args);                                    /*Verifica sensores em 12 Hz*/
void desligaThreads(void);                                                      /*Interrompe as Threads*/
void iniciaThreads(void);                                                       /*Inicia as Threads*/
void exibeLogo(void);                                                           /*Exibe o logo da equipe*/
void threadAquisicaoDados(void);                                                /*Verifica se a leitura dos sensores esta em 12Hz*/

/*Declara��o das Threads*/
osThreadDef(contagemFall, osPriorityNormal, 1,0);
osThreadDef(atualizaDisplay,  osPriorityBelowNormal, -1,0);
osTimerDef(sensorsTimer, verficaSensoresTimer);
osThreadDef(gravaSD,  osPriorityNormal, 1,0);


/*ThreadIds*/
osThreadId t_fall;
osThreadId t_display;
osThreadId t_grava;

/*TimerIds*/
osTimerId sensors;


/*MutexIds*/
osMutexId std_mutex;
osMutexDef(std_mutex);

int main (void)
{
    inicia();  
    bool run = true;     
    uint32_t timeGantt;
    int ax, bx; //Para depois fazer tratamento de erros
    ax = verificaSD();    
    bx =  criaArquivo();
    oled_clearScreen(OLED_COLOR_WHITE);
    
    /*Criando as Threads na main*/
    sensors= osTimerCreate(osTimer(sensorsTimer), osTimerPeriodic, NULL);
    t_fall = osThreadCreate(osThread(contagemFall), NULL);
    t_display = osThreadCreate(osThread(atualizaDisplay), NULL);
    t_grava = osThreadCreate(osThread(gravaSD), NULL);
    
    /*Mutex*/
    std_mutex = osMutexCreate(osMutex(std_mutex));
    
    /*Verifica se o cart�o SD est� na placa*/
    if(ax==0 && bx==0)
    {
        exibeLogo();
        osDelay(100);
        iniciaDispaly(true);   
        run = true; 
        
        osDelay(100);
        
        /* Cria o arquivo gantt */
        f_open(&fil_Gantt, "gantt.txt", FA_READ | FA_CREATE_ALWAYS | FA_WRITE);  //Fun�ao para abrir arquivo          
        sprintf(linegantt,"    Diagrama de Gantt:  Laboratorio 3\n");
        f_puts(linegantt, &fil_Gantt);  
        f_sync(&fil_Gantt);//Para gravar s� uma vez
        sprintf(linegantt,"    Vin�cius Sandri Diaz - S12\n");
        f_puts(linegantt, &fil_Gantt);  
        f_close(&fil_Gantt);//Para gravar s� uma vez
    }
    
    else
    {
        iniciaDispaly(false);
        run = false;
    }
    
    if(run)
    {
        /*Calibra os sensores*/
        acc_read(&x, &y, &z);
        xoff = 0-x;
        yoff = 0-y;
        zoff = 64-z;
        
        light_enable();
        light_setRange(LIGHT_RANGE_4000);
        
        osDelay(20);
        
        while(1) 
        {         
            osDelay(20);  
            
            if(run)        
                iniciaThreads(); //Utiliza interrup��o para iniciar as threads           
                
                if(fall)
                    run = false;       
                
                if(GPIOGetValue(2,9)==0)
                {
                    desligaThreads();  //Utiliza interrup��o para pausar as threads 
                    run = false;
                }
                
                if(GPIOGetValue(1,4)==0 && (!run || fall))
                {
                    iniciaDispaly(true);
                    run = true;
                }      
                
                osDelay(100);  
        } 
    }  
}

/*Fun��es para iniciar Threads, timer e mutex*/

void display_thread(void const *args)
{
    osSignalSet(t_display, 0x1);
}

void fall_thread(void const *args) 
{
    osSignalSet(t_fall, 0x1);  
}

void grava_thread(void const *args)
{
    osSignalSet(t_grava, 0x1);
}

/*Interrompe as Threads*/
void desligaThreads(void)
{
    
    pca9532_setLeds(0x0000, 0xffff); 
    oled_clearScreen(OLED_COLOR_BLACK);
    osTimerStop(sensors);
    
    osSignalClear (t_display, 0x01);
    osSignalClear (t_grava, 0x01);
    osSignalClear (t_fall, 0x01);
}

/*Inicia as Threads*/
void iniciaThreads(void)
{
    osTimerStart(sensors,  80);
    osKernelStart();  
    display_thread(NULL);  
    fall_thread(NULL);
}  
 /*Verifica se a leitura dos sensores esta em 12Hz*/
void threadAquisicaoDados(void)
{
    
    timerAcq[i]=osKernelSysTick()/72; //ticks em micro segundos  
    if(i==8)
        timerAcq[i]=0;  //coloco break point aqui para verificar o tempo
        i++;
    
}
void inicia(void)
{
    
    SystemInit(); 
    SystemCoreClockUpdate();
    osKernelInitialize();
    
    GPIOInit();    
    UARTInit(115200);
    UARTSendString((uint8_t*)"OLED - Peripherals\r\n");
    I2CInit( (uint32_t)I2CMASTER, 0 );
    SSPInit();
    oled_init();
    light_init();
    acc_init();
    led7seg_init();
    rgb_init();
    iniciaData();    
} 
/*Fun��o para gravar buffer circular no sd*/
void gravaSD(void const *args)
{
    uint32_t timeGantt;
    
    
    while (1) 
    {
        osSignalWait(0x1, osWaitForever);    
        timeGantt = osKernelSysTick()/ticks_factor;
        
        if(posiUltimo ==(BUFFSIZE-1))
        {
            
            osMutexWait(std_mutex, osWaitForever );
            /*Funcoes FatFs para gravar arquivo data.txt*/    
            f_open(&fil_Grava, "data.csv", FA_READ | FA_WRITE);  //Fun�ao para abrir arquivo
            pca_toggle(5);
            
            /*verfica arquivos j� gravados*/
            while (f_gets(line2, sizeof line2, &fil_Grava));
            
            uint8_t k =0;
            
            for(int j =0; j <BUFFSIZE; j++)
            {
                sprintf(converte, "%d", circBuff[j]);
                
                if(k < 3)
                {
                    sprintf(line2  + strlen(line2), "%s, ", converte);
                    k++;
                }
                
                else if(k ==3)
                {
                    sprintf(line2  + strlen(line2), "%s\n", converte);
                    k = 0;
                }            
                
            }
            posiUltimo = 0;
            osMutexRelease(std_mutex);
            f_puts(line2, &fil_Grava);  
            f_sync(&fil_Grava);//Para gravar s� uma vez
            f_close(&fil_Grava);//Fecha o arquivo   
        } 
        
        osMutexWait(std_mutex, osWaitForever );
        f_open(&fil_Gantt, "gantt.txt", FA_READ | FA_WRITE);  //Fun�ao para abrir arquivo
        
        while(f_gets(linegantt, sizeof linegantt, &fil_Gantt));
        sprintf(linegantt, "Grava:    %d, %d\n",  timeGantt, osKernelSysTick()/ticks_factor);
        f_puts(linegantt, &fil_Gantt);  
        f_sync(&fil_Gantt);//Para gravar s� uma vez
        f_close(&fil_Gantt);//Para gravar s� uma vez
        osMutexRelease(std_mutex);
    }
}

/*Adiciona m�dia m�vel no buffer circular*/
void addBuff(int32_t elemx, int32_t elemy, int32_t elemz, int32_t eleml)
{
    
    osMutexWait(std_mutex, osWaitForever );
    
    if(countBuf < 29)
    {
        /*Adicionando media movel no bufferCircula*/
        circBuff[countBuf] = elemx;
        circBuff[countBuf +1] = elemy;
        circBuff[countBuf +2] = elemz;
        circBuff[countBuf +3] = eleml;
        posiUltimo = countBuf +3;
        countBuf += 4;
    } 
    
    else
    {
        countBuf = 0;   
        osSignalSet(t_grava, 0x1); 
    } 
    
    osMutexRelease(std_mutex); 
    
}

/*Calcula M�dia M�vel*/
void mediaMovel(int32_t xs,  int32_t ys,  int32_t zs,  int32_t zl)
{   
    
    if(count<4)
    {
        _x += xs;
        _y += ys;
        _z += zs;
        _l += zl;
        count++; 
        
        if (count == 4)
        {
            _x = _x/4;
            _y = _y/4;
            _z = _z/4;
            _l = _l/4;
            
            osMutexWait(std_mutex, osWaitForever );
            addBuff(_x, _y, _z, _l);       
            osMutexRelease(std_mutex);
            count++;    
        }            
    }
    
    else        
    {
        count =0;    
        _x = 0;
        _y = 0;
        _z = 0;    
        _l = 0;
    }   
    
} 
/*Adiciona se a placa esta em queda*/
void setFall(bool _fall)
{
    fall = _fall;    
}

/*Inicia as configura��es do display*/
void iniciaDispaly(const bool inicia)
{
    
    oled_clearScreen(OLED_COLOR_WHITE);  
    osDelay(50);
    
    if(!inicia)
    {
        oled_putString(1,9,  (uint8_t*)" NO SD Card! ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,17,  (uint8_t*)" NO Arquivo! ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,25, (uint8_t*)" Verefique o SD", OLED_COLOR_BLACK, OLED_COLOR_WHITE);   
    }
    
    else
    { 
        oled_putString(1,1,  (uint8_t*)"Vinicius - S12", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,9,  (uint8_t*)"Light  : ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,17, (uint8_t*)"Time   : ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,25, (uint8_t*)"Acc x  : ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,33, (uint8_t*)"Acc y  : ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,41, (uint8_t*)"Acc z  : ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);   
    }
}

/*Atualiza o display*/
void atualizaDisplay(void const *args)
{ 
    uint32_t timeGantt;
    
    while (1) 
    {
        
        timeGantt = osKernelSysTick()/ticks_factor;
        osMutexWait(std_mutex, osWaitForever );
        osSignalWait(0x1, osWaitForever);  
        
        if(posiUltimo > 0)
        {
            intToString(circBuff[posiUltimo], buf, 10, 10);
            oled_fillRect((1+9*6),9, 80, 16, OLED_COLOR_WHITE);
            oled_putString((1+9*6),9, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);
            
            intToString(time, buf, 10, 10);
            oled_fillRect((1+9*6),17, 80, 24, OLED_COLOR_WHITE);
            oled_putString((1+9*6),17, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);
            
            intToString(circBuff[posiUltimo-3], buf, 10, 10);
            oled_fillRect((1+9*6),25, 80, 32, OLED_COLOR_WHITE);
            oled_putString((1+9*6),25, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);
            
            intToString(circBuff[posiUltimo-2], buf, 10, 10);
            oled_fillRect((1+9*6),33, 80, 40, OLED_COLOR_WHITE);
            oled_putString((1+9*6),33, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);
            
            intToString(circBuff[posiUltimo-1], buf, 10, 10);
            oled_fillRect((1+9*6),41, 80, 48, OLED_COLOR_WHITE);
            oled_putString((1+9*6),41, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);  
        }   
        osMutexRelease(std_mutex);
        
        osMutexWait(std_mutex, osWaitForever );
        f_open(&fil_Gantt, "gantt.txt", FA_READ | FA_WRITE);  //Fun�ao para abrir arquivo
        while(f_gets(linegantt, sizeof linegantt, &fil_Gantt));
        sprintf(linegantt, "Display:  %d, %d\n",  timeGantt, osKernelSysTick()/ticks_factor);
        f_puts(linegantt, &fil_Gantt);  
        f_sync(&fil_Gantt);//Para gravar s� uma vez
        f_close(&fil_Gantt);//Para gravar s� uma vez
        osMutexRelease(std_mutex);
        
        osDelay(300);     
    }
}
 /*Contagem da placa na vertical*/
void contagemFall(void const *args)
{
    bool flag = false;
    uint32_t timeGantt;
    
    while (1) 
    {
        // Signal flags that are reported as event are automatically cleared.
        setFall(false); 
        osSignalWait(0x1, osWaitForever);    
        timeGantt = osKernelSysTick()/ticks_factor;  
        
        
        if(( y > 55 || y < (-55)) && !flag)
        {
            time = osKernelSysTick()/ticks_factor;  
            flag = true;  
            
        }
        
        else if(( y > 55 || y < (-55)) && flag) 
        {
            flag = false;
            time = 0;
        }  
        
        while((osKernelSysTick()/ticks_factor -  time) < 3000 && flag);
        
        if(( y > 55 || y < (-55))  && flag)
        {
            
            setFall(true);
            desligaThreads();   
            break;               
        }
        
        osMutexWait(std_mutex, osWaitForever );
        
        f_open(&fil_Gantt, "gantt.txt", FA_READ | FA_WRITE);  //Fun�ao para abrir arquivo
        while(f_gets(linegantt, sizeof linegantt, &fil_Gantt));
        sprintf(linegantt, "Contagem: %d, %d\n",  timeGantt, osKernelSysTick()/ticks_factor);
        f_puts(linegantt, &fil_Gantt);  
        f_sync(&fil_Gantt);//Para gravar s� uma vez
        f_close(&fil_Gantt);//Para gravar s� uma vez
        
        osMutexRelease(std_mutex);
    }
    
}

/*Verifica sensores em 12 Hz*/
void verficaSensoresTimer(void const *args)
{ 
    /* Accelerometer */
    osMutexWait(std_mutex, osWaitForever );
    timeGanttTimer = osKernelSysTick()/ticks_factor;
    acc_read(&x, &y, &z);
    x = x+xoff;
    y = y+yoff;
    z = z+zoff;
    lux = light_read();
    
    mediaMovel(x,y,z, lux);    
    osMutexRelease(std_mutex);
    
    pca_toggle(10);
    //threadAquisicaoDados();
}

/*Exibe o logo da equipe*/
void exibeLogo(void)
{
    uint8_t linha, coluna;
    char vetorImagem[1];
    
    f_open(&fil_Grava, "archLogo.ppm", FA_READ);  //Fun�ao para abrir arquivo
    f_gets( vetorImagem, 0x32, &fil_Grava); 
    
    for(linha=0; linha < 64; linha++)              //Percorre o vetor
        for(coluna=0;coluna < 96; coluna++)
        {
            if(vetorImagem[0] > 100)                     
                oled_putPixel(coluna, linha, 1);          
            else
                oled_putPixel(coluna, linha, 0);            
            
            f_gets( vetorImagem, 4, &fil_Grava);                 //Incrmenta o vetor do endere�o da imagem tratada
        }
        
        osDelay(1500);
        f_close(&fil_Grava);//Fecha o arquivo   
}


