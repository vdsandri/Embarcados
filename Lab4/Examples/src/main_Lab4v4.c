/*============================================================================
 *           LPCXpresso 1343 + Embedded Artists Development Board            *
 *---------------------------------------------------------------------------*
 *            Universidade Tecnol�gica Federal do Paran� (UTFPR)             *
 *                                                                           *
 *              Lab 4: Aluno Vin�cius Sandri Diaz                            *   
 *              Equipe 8 -S12                                                *
 *===========================================================================*/

#include "mcu_regs.h"
#include "libdemo.h"
#include "type.h"
#include "uart.h"
#include "stdio.h"
#include "timer32.h"
#include "i2c.h"
#include "gpio.h"
#include "ssp.h"
#include "adc.h"
#include "light.h"
#include "oled.h"
#include "rgb.h"
#include "temp.h"
#include "acc.h"
#include "data.h"
#include "stdbool.h"
#include "diskio.h"
#include "ff.h"
#include "cmsis_os.h"
#include "modbus_protocol.h"


#define readRegSlave 0x08
#define BUFFSIZE 32

const uint32_t ticks_factor = 72000;
static int32_t circBuff[BUFFSIZE];

static char line2[330];
static char linegantt[50];
static char converte[10];



/*Utilizados para calibrar o acelerometro*/
int32_t xoff = 0;
int32_t yoff = 0;
int32_t zoff = 0;

int8_t x = 0;
int8_t y = 0;
int8_t z = 0;
uint32_t lux = 0;
uint32_t timeGanttTimer =0;

static  FIL fil_Grava;         /* File object */
static  FIL fil_Gantt;         /* File object */

/*Variaveis utilizadas em algumas fun��es*/

uint32_t timerAcq[8];
uint8_t countBuf = 0;
uint8_t count = 0;
uint8_t posiUltimo = 0;


/*Variaveis para armazenar no buffer circular*/
int32_t _x;
int32_t _y;
int32_t _z;
uint32_t _l;
uint32_t time;  


int8_t g_byteMsgBuffer[8];//vetor de dados
uint8_t g_iMsgIndex=0; //iterador
Master_Req * pMsg = (Master_Req*)&g_byteMsgBuffer;
EState state=WAITING_START_MSG;
uint8_t bufferRsp[10];   //buffer de resposta (global)
uint8_t bufferRspTam;//tamanho do buffer de resposta
uint8_t iterBuffer=0;
uint8_t tamMsg; /*Tamanho em bytes da mensagem recebida*/



uint16_t crc;//crc da mensagem
uint16_t endComando;//endereo da mensagem
uint16_t dados;//dados da mensagem
uint16_t registers[16];//registradores da placa


//dados da funo 03:readmultipleregisters
uint8_t i;
uint8_t qtdValores;
uint8_t endereco;


/*Fun��es Implementadas no Projeto*/
void processaMensagemRecebida(void);
void respostaBuffer(void);
void recebeDados(void const *args);
void switchLed(uint16_t endLed, uint16_t opcao);
void switchJoystick(void);
void inicia(void);
void gravaSD(void const *args);                                                 /*Fun��o para gravar buufer circular no sd*/
void addBuff(int32_t elemx, int32_t elemy, int32_t elemz,  int32_t eleml);      /*Adiciona m�dia m�vel no buffer circular*/
void mediaMovel(int32_t xs,  int32_t ys,  int32_t zs, int32_t zl);              /*Calcula M�dia M�vel*//*Inicia fun��es da placa*/
void iniciaDispaly(const bool inicia);                                          /*Inicia o dislplay*/
void atualizaDisplay(void const *args);                                         /*Atualiza o display*/
void verficaSensoresTimer(void const *args);                                    /*Verifica sensores em 12 Hz*/
void desligaThreads(void);                                                      /*Interrompe as Threads*/
void iniciaThreads(void);                                                       /*Inicia as Threads*/
void exibeLogo(void);                                                           /*Exibe o logo da equipe*/
void threadAquisicaoDados(void);                                                /*Verifica se a leitura dos sensores esta em 12Hz*/

/*Declara��o das Threads*/
osThreadDef(atualizaDisplay,  osPriorityNormal, 1,0);
osThreadDef(recebeDados,  osPriorityNormal, 1,0);
osTimerDef(sensorsTimer, verficaSensoresTimer);
osThreadDef(gravaSD,  osPriorityNormal, 1,0);



/*ThreadIds*/
osThreadId t_display;
osThreadId t_grava;
osThreadId t_serial;


/*TimerIds*/
osTimerId sensors;


/*MutexIds*/
osMutexId std_mutex;
osMutexDef(std_mutex);


void recebeDados(void const *args)

{
    /* Para receber mensagem */ 
    
    uint32_t timeout;
    
    while(1)
    {
       timeout =  osKernelSysTick()/ticks_factor;

        switch(state)
        {//maquina de recepo ENLACE
            case WAITING_START_MSG://caso exista um start bit especfico
                //se chegar dados seriais muda de estado
                //ler primeiro negocio e verificar se a msg eh pra mim
                state=WAITING_END_MSG;
                break;
            case WAITING_END_MSG://espera termino da mensagem
                GPIOSetValue(PORT1, 9, 0);
                tamMsg = UARTReceive(&(g_byteMsgBuffer[g_iMsgIndex]), 8, FALSE);
                g_iMsgIndex +=tamMsg;
                
                
                if(g_iMsgIndex == 8)// && (osKernelSysTick()/ticks_factor - timeout)<4)
                    /*Caso no receba a msg inteira em 100 mile*/ 
                    {
                        crc = usMBCRC16(g_byteMsgBuffer,6);
                        // se fim-> calcula CRC -> se OK -> manda p/ processamento
                        if (crc == (pMsg->CRCMSB << 8) | pMsg->CRCLSB && pMsg->endereco == 8) 
                        {
                            processaMensagemRecebida();                
                            iterBuffer=0; //zera index para nova msg
                        } // se NOK -> no faz nada
                        
                        g_iMsgIndex = 0;
                    }
                    
                    break;
        }
    }
}


//funo para processar a mendagem recebida
void processaMensagemRecebida(void)
{  //maquina da camada de aplicao
    endComando = (pMsg->endComandoMSB << 8) | pMsg->endComandoLSB;
    dados = (pMsg->dadosMSB << 8) | pMsg->dadosLSB;
    
    
    qtdValores = (pMsg->dadosMSB << 8) | pMsg->dadosLSB;
    endereco = (pMsg->endComandoMSB << 8) | pMsg->endComandoLSB;
    
    switch(pMsg->codFuncao){
      
        case 2:
          
              bufferRsp[iterBuffer++]= 8;
              bufferRsp[iterBuffer++]=pMsg->codFuncao;
              bufferRsp[iterBuffer++]= registers[4];
              bufferRsp[iterBuffer++]=pMsg->endComandoLSB;
              bufferRsp[iterBuffer++]=pMsg->dadosMSB;
              bufferRsp[iterBuffer++]=pMsg->dadosLSB;
              crc = usMBCRC16(bufferRsp,iterBuffer); //calcularCRC
              bufferRsp[iterBuffer++] = crc & 0xFF;
              bufferRsp[iterBuffer++] = crc >> 8;
              bufferRspTam = 8;
              
               respostaBuffer();
          
          break;
          
      
        case 5:// Func : write single coil 
            
            switchLed(endComando, dados);
            respostaBuffer();
            
            break;
        case 6:// Func : write single register 
            
            if(endereco == 15)
            { //caso for escrever no tempo de amostragem, precisa testar
                registers[endComando] = dados;
            }
            switchLed(endComando, dados);
            respostaBuffer();
            
            break;
            
            
        case 0x03:// Func : write single register
            
            iterBuffer=0;
            
            //Colocando que escravo esta respondendo
            bufferRsp[iterBuffer++]=pMsg->endereco;
            
            //Colocando que tipo de requisicao estou respondendo
            bufferRsp[iterBuffer++]=pMsg->codFuncao;
            
            //Colocando na terceira posio a quantidade de informaes que tem que ler
            bufferRsp[iterBuffer++]= 2*qtdValores;
            
            for(i = 0; i <qtdValores ; i++)
            {
                
                bufferRsp[iterBuffer+1] = registers[endereco + i] ;
                bufferRsp[iterBuffer] = registers[endereco + i] >> 8;
                
                iterBuffer =  iterBuffer + 2;
            }   
            
            crc = usMBCRC16(bufferRsp, iterBuffer);
            
            bufferRsp[iterBuffer++] = crc;
            bufferRsp[iterBuffer++] = crc >> 8; 
            
            bufferRspTam = iterBuffer;
            respostaBuffer();
            
            break;
            
    }
}

void respostaBuffer(void)
{
    GPIOSetValue( PORT1, 9,1);
    osDelay(10);
    UARTSend(bufferRsp,bufferRspTam);
    osDelay(10);

}


//funo para acionar/desligar os LEDs
void switchLed(uint16_t endLed, uint16_t opcao)
{
    switch(endLed){
        case 0x00:
            //toggle led 4
            if(opcao==0x00)
            {
                registers[endLed] = 0x0;
                pca9532_setLeds (0x0000, LED4);
            }
            else
            {
                registers[endLed] = 0x0100;
                pca9532_setLeds (LED4, 0x0000);
            }
            break;
        case 0x01:
            //toggle led 5
            if(opcao==0x00)
            {
                registers[endLed] = 0x0;
                pca9532_setLeds (0x0000, LED5);
            }
            else
            {
                registers[endLed] = 0x0100;
                pca9532_setLeds (LED5, 0x0000);
            }
            break;
        case 0x02:
            //toggle led 19
            if(opcao==0x00){
                registers[endLed] =0x0;
                pca9532_setLeds (0x0000, LED19);
            }
            else{
                registers[endLed] = 0x0100;
                pca9532_setLeds (LED19, 0x0000);
            }
            break;
        case 0x03:
            //toggle led 18
            if(opcao==0x00)
            {
                registers[endLed] = 0x0;
                pca9532_setLeds (0x0000, LED18);
            }
            else
            {
                registers[endLed] = 0x0100;
                pca9532_setLeds (LED18, 0x0000);
            }
            break;
            
            
         case 0x04:
            //toggle led 18
            if(opcao==0x00)
            {
                registers[endLed] = x;
            }
            else
            {
                registers[endLed] = x;
            }    
            
        default:
            break;
    }
    
    bufferRsp[iterBuffer++]= 8;
    bufferRsp[iterBuffer++]=pMsg->codFuncao;
    bufferRsp[iterBuffer++]=pMsg->endComandoMSB;
    bufferRsp[iterBuffer++]=pMsg->endComandoLSB;
    bufferRsp[iterBuffer++]=pMsg->dadosMSB;
    bufferRsp[iterBuffer++]=pMsg->dadosLSB;
    crc = usMBCRC16(bufferRsp,iterBuffer); //calcularCRC
    bufferRsp[iterBuffer++] = crc & 0xFF;
    bufferRsp[iterBuffer++] = crc >> 8;
    bufferRspTam = 8;
    
}

void switchJoystick(void)
{
 
  if (!GPIOGetValue(PORT2, 0))
  
  {
      registers[10] = 0x0001;//center
  }
  else
  {
     registers[10] = 0x0000;//center
  }
  if (!GPIOGetValue(PORT2, 1)){
      registers[9] = 0x0001; //down          
  }
  else
  {
     registers[9] = 0x0000;//down
  }
  
  if (!GPIOGetValue(PORT2, 2))
  {
      registers[11] = 0x0001; //right    
  } 
  else
  {
     registers[11] = 0x0000;//right
  }
  if (!GPIOGetValue(PORT2, 3))
  {
      registers[8] = 0x0001; //up
  }
  else
  {
     registers[8] = 0x0000;//up
  }
  if (!GPIOGetValue(PORT2, 4))
  {
      registers[12] = 0x0001; //left
  }
  else
  {
     registers[12] = 0x0000;//left
  }

}

int main (void)
{
    inicia();  
    bool run = true;     
    uint32_t timeGantt;
    int ax, bx; //Para depois fazer tratamento de erros
    ax = verificaSD();    
    bx =  criaArquivo();
    oled_clearScreen(OLED_COLOR_WHITE);
    registers[15] = 1000; 
    
    /*Criando as Threads na main*/
    sensors= osTimerCreate(osTimer(sensorsTimer), osTimerPeriodic, NULL);
    t_display = osThreadCreate(osThread(atualizaDisplay), NULL);
    t_serial = osThreadCreate(osThread(recebeDados), NULL);
    t_grava = osThreadCreate(osThread(gravaSD), NULL);
    
    /*Mutex*/
    std_mutex = osMutexCreate(osMutex(std_mutex));
    
    /*Verifica se o cart�o SD est� na placa*/
    if(ax==0 && bx==0)
    {
        exibeLogo();
        osDelay(200);
        iniciaDispaly(true);   
        run = true; 
        
        
        //        /* Cria o arquivo gantt */
        //        f_open(&fil_Gantt, "gantt.txt", FA_READ | FA_CREATE_ALWAYS | FA_WRITE);  //Fun�ao para abrir arquivo          
        //        sprintf(linegantt,"    Diagrama de Gantt:  Laboratorio 3\n");
        //        f_puts(linegantt, &fil_Gantt);  
        //        f_sync(&fil_Gantt);//Para gravar s� uma vez
        //        sprintf(linegantt,"    Vin�cius Sandri Diaz - S12\n");
        //        f_puts(linegantt, &fil_Gantt);  
        //        f_close(&fil_Gantt);//Para gravar s� uma vez
    }
    
    else
    {
        iniciaDispaly(false);
        run = false;
    }
    
    if(run)
    {
        /*Calibra os sensores*/
        acc_read(&x, &y, &z);
        xoff = 0-x;
        yoff = 0-y;
        zoff = 64-z;
        
        light_enable();
        light_setRange(LIGHT_RANGE_4000);
        osTimerStart(sensors,   registers[15]);
        osKernelStart();  
        
        while(1) 
        {         
            //recebeDados();
            if(GPIOGetValue(2,9)==0)
            {
                desligaThreads();  //Utiliza interrup��o para pausar as threads 
                run = false;
            }
            
            if(GPIOGetValue(1,4)==0 && !run)
            {
                iniciaDispaly(true);
                run = true;
            }   
            
            osDelay(100);  
        } 
    }  
}



/*Fun��es para iniciar Threads, timer e mutex*/
void serial_thread(void const *args)
{
    osSignalSet(t_serial, 0x1);
}
void display_thread(void const *args)
{
    osSignalSet(t_display, 0x1);
}

/*Interrompe as Threads*/
void desligaThreads(void)
{ 
    pca9532_setLeds(0x0000, 0xffff); 
    oled_clearScreen(OLED_COLOR_BLACK);
    osTimerStop(sensors);    
    osSignalClear (t_display, 0x01);
    
}

/*Inicia as Threads*/
void iniciaThreads(void)
{
    osTimerStart(sensors,  800);
    osKernelStart();  
    display_thread(NULL);  
    serial_thread(NULL);
}  
/*Verifica se a leitura dos sensores esta em 12Hz*/
void threadAquisicaoDados(void)
{
    
    timerAcq[i]=osKernelSysTick()/72; //ticks em micro segundos  
    if(i==8)
        timerAcq[i]=0;  //coloco break point aqui para verificar o tempo
        i++;
    
}
void inicia(void)
{
    
    SystemInit(); 
    SystemCoreClockUpdate();
    osKernelInitialize();
    
    GPIOInit();    
    UARTInit(9600);
    UARTSendString((uint8_t*)"OLED - Peripherals\r\n");
    I2CInit( (uint32_t)I2CMASTER, 0 );
    SSPInit();
    oled_init();
    light_init();
    acc_init();
    rgb_init();
    iniciaData();   
    
    GPIOSetDir(PORT1, 9,1);
} 

/*Inicia as configura��es do display*/
void iniciaDispaly(const bool inicia)
{
    
    oled_clearScreen(OLED_COLOR_WHITE);  
    osDelay(50);
    
    if(!inicia)
    {
        oled_putString(1,9,  (uint8_t*)" NO SD Card! ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,17,  (uint8_t*)" NO Arquivo! ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,25, (uint8_t*)" Verefique o SD", OLED_COLOR_BLACK, OLED_COLOR_WHITE);   
    }
    
    else
    { 
        oled_putString(1,1,  (uint8_t*)"Vinicius - S12", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,9,  (uint8_t*)"Light  : ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,17, (uint8_t*)"Time   : ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,25, (uint8_t*)"Acc x  : ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,33, (uint8_t*)"Acc y  : ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);
        oled_putString(1,41, (uint8_t*)"Acc z  : ", OLED_COLOR_BLACK, OLED_COLOR_WHITE);   
    }
}

/*Atualiza o display*/
void atualizaDisplay(void const *args)
{ 
    uint32_t timeGantt;
    
    while (1) 
    {
        
        //timeGantt = osKernelSysTick()/ticks_factor;
        timeGantt = 0;
        
        //osMutexWait(std_mutex, osWaitForever );
        //osSignalWait(0x1, osWaitForever);  
        if(countBuf> 0)
        {
            intToString(lux, buf, 10, 10);
            oled_fillRect((1+9*6),9, 80, 16, OLED_COLOR_WHITE);
            oled_putString((1+9*6),9, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);
            
            intToString(registers[15], buf, 10, 10);
            oled_fillRect((1+9*6),17, 80, 24, OLED_COLOR_WHITE);
            oled_putString((1+9*6),17, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);
            
            intToString(x, buf, 10, 10);
            oled_fillRect((1+9*6),25, 80, 32, OLED_COLOR_WHITE);
            oled_putString((1+9*6),25, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);
            
            intToString(y, buf, 10, 10);
            oled_fillRect((1+9*6),33, 80, 40, OLED_COLOR_WHITE);
            oled_putString((1+9*6),33, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);
            
            intToString(z, buf, 10, 10);
            oled_fillRect((1+9*6),41, 80, 48, OLED_COLOR_WHITE);
            oled_putString((1+9*6),41, buf, OLED_COLOR_BLACK, OLED_COLOR_WHITE);  
            timeGantt++;
            
            osDelay(300);
        }  
        
    }
    
    //osMutexRelease(std_mutex);
    
    //        osMutexWait(std_mutex, osWaitForever );
    //        f_open(&fil_Gantt, "gantt.txt", FA_READ | FA_WRITE);  //Fun�ao para abrir arquivo
    //        while(f_gets(linegantt, sizeof linegantt, &fil_Gantt));
    //        sprintf(linegantt, "Display:  %d, %d\n",  timeGantt, osKernelSysTick()/ticks_factor);
    //        f_puts(linegantt, &fil_Gantt);  
    //        f_sync(&fil_Gantt);//Para gravar s� uma vez
    //        f_close(&fil_Gantt);//Para gravar s� uma vez
    //        osMutexRelease(std_mutex);
    
}


/*Verifica sensores em 12 Hz*/
void verficaSensoresTimer(void const *args)
{ 
    /* Accelerometer */
    // osMutexWait(std_mutex, osWaitForever );
    // timeGanttTimer = osKernelSysTick()/ticks_factor;
    acc_read(&x, &y, &z);
    x = x+xoff;
    y = y+yoff;
    z = z+zoff;
    lux = light_read();
    mediaMovel(x,y,z, lux);
    registers[4]=x; 
    registers[5]=y;  
    registers[6]=z; 
    registers[7]=lux; 
    switchJoystick();
    
    // osMutexRelease(std_mutex);
    
    //threadAquisicaoDados();
}

/*Fun��o para gravar buffer circular no sd*/
void gravaSD(void const *args)
{
    uint32_t timeGantt;
    
    
    while (1) 
    {
        //osSignalWait(0x1, osWaitForever);    
        timeGantt = osKernelSysTick()/ticks_factor;
        
        if((posiUltimo+1) % 4 == 0)
        {
            
           // osMutexWait(std_mutex, osWaitForever );
            /*Funcoes FatFs para gravar arquivo data.txt*/    
            f_open(&fil_Grava, "data.csv", FA_READ | FA_WRITE);  //Fun�ao para abrir arquivo
            pca9532_setLeds (LED11, 0x0000);
            /*verfica arquivos j� gravados*/
            while (f_gets(line2, sizeof line2, &fil_Grava));
            
            uint8_t k =0;
            
            for(int j =0; j <BUFFSIZE; j++)
            {
                sprintf(converte, "%d", circBuff[j]);
                
                if(k < 3)
                {
                    sprintf(line2  + strlen(line2), "%s, ", converte);
                    k++;
                }
                
                else if(k ==3)
                {
                    sprintf(line2  + strlen(line2), "%s\n", converte);
                    k = 0;
                }            
                
            }
            posiUltimo = 0;
            //osMutexRelease(std_mutex);
            f_puts(line2, &fil_Grava);  
            f_sync(&fil_Grava);//Para gravar s� uma vez
            f_close(&fil_Grava);//Fecha o arquivo   
            pca9532_setLeds (0x0000, LED11);
        } 
        
        //        osMutexWait(std_mutex, osWaitForever );
        //        f_open(&fil_Gantt, "gantt.txt", FA_READ | FA_WRITE);  //Fun�ao para abrir arquivo
        //        
        //        while(f_gets(linegantt, sizeof linegantt, &fil_Gantt));
        //        sprintf(linegantt, "Grava:    %d, %d\n",  timeGantt, osKernelSysTick()/ticks_factor);
        //        f_puts(linegantt, &fil_Gantt);  
        //        f_sync(&fil_Gantt);//Para gravar s� uma vez
        //        f_close(&fil_Gantt);//Para gravar s� uma vez
        //        osMutexRelease(std_mutex);
    }
}

/*Adiciona m�dia m�vel no buffer circular*/
void addBuff(int32_t elemx, int32_t elemy, int32_t elemz, int32_t eleml)
{
    
    //osMutexWait(std_mutex, osWaitForever );
    
    if(countBuf < 29)
    {
        /*Adicionando media movel no bufferCircula*/
        circBuff[countBuf] = elemx;
        circBuff[countBuf +1] = elemy;
        circBuff[countBuf +2] = elemz;
        circBuff[countBuf +3] = eleml;
        posiUltimo = countBuf +3;
        countBuf += 4;
    } 
    
    else
    {
        countBuf = 0;   
        osSignalSet(t_grava, 0x1); 
    } 
    
    // osMutexRelease(std_mutex); 
    
}

/*Calcula M�dia M�vel*/
void mediaMovel(int32_t xs,  int32_t ys,  int32_t zs,  int32_t zl)
{   
    
    if(count<4)
    {
        _x += xs;
        _y += ys;
        _z += zs;
        _l += zl;
        count++; 
        
        if (count == 4)
        {
            _x = _x/4;
            _y = _y/4;
            _z = _z/4;
            _l = _l/4;
            
            // osMutexWait(std_mutex, osWaitForever );
            addBuff(_x, _y, _z, _l);       
            // osMutexRelease(std_mutex);
            count++;    
        }            
    }
    
    else        
    {
        count =0;    
        _x = 0;
        _y = 0;
        _z = 0;    
        _l = 0;
    }   
    
} 


/*Exibe o logo da equipe*/
void exibeLogo(void)
{
   oled_clearScreen(OLED_COLOR_BLACK);
    uint8_t linha, coluna;
    char vetorImagem[1];
    
    f_open(&fil_Grava, "archLogo.ppm", FA_READ);  //Fun�ao para abrir arquivo
    f_gets( vetorImagem, 0x32, &fil_Grava); 
    
    for(linha=0; linha < 64; linha++)              //Percorre o vetor
        for(coluna=0;coluna < 96; coluna++)
        {
            if(vetorImagem[0] > 100)                     
                oled_putPixel(coluna, linha, 1);          
            else
                oled_putPixel(coluna, linha, 0);            
            
            f_gets( vetorImagem, 4, &fil_Grava);                 //Incrmenta o vetor do endere�o da imagem tratada
        }
        
        osDelay(1500);
    
        f_close(&fil_Grava);//Fecha o arquivo   
}


